EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:conn
LIBS:tag_connect_swd-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X10 P1
U 1 1 57AAC984
P 4250 3850
F 0 "P1" H 4250 4400 50  0000 C CNN
F 1 "CONN_02X10" V 4250 3850 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Angled_2x10" H 4250 2650 50  0001 C CNN
F 3 "" H 4250 2650 50  0000 C CNN
	1    4250 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 57AACA9C
P 4600 4400
F 0 "#PWR01" H 4600 4150 50  0001 C CNN
F 1 "GND" H 4600 4250 50  0000 C CNN
F 2 "" H 4600 4400 50  0000 C CNN
F 3 "" H 4600 4400 50  0000 C CNN
	1    4600 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3500 4600 3500
Wire Wire Line
	4600 3500 4600 4400
Wire Wire Line
	4500 3600 4600 3600
Connection ~ 4600 3600
Wire Wire Line
	4600 3700 4500 3700
Connection ~ 4600 3700
Wire Wire Line
	4500 3800 4600 3800
Connection ~ 4600 3800
Wire Wire Line
	4600 3900 4500 3900
Connection ~ 4600 3900
Wire Wire Line
	4500 4000 4600 4000
Connection ~ 4600 4000
Wire Wire Line
	4500 4100 4600 4100
Connection ~ 4600 4100
Wire Wire Line
	4500 4200 4600 4200
Connection ~ 4600 4200
Wire Wire Line
	4500 4300 4600 4300
Connection ~ 4600 4300
$Comp
L VCC #PWR02
U 1 1 57AACB30
P 3900 3300
F 0 "#PWR02" H 3900 3150 50  0001 C CNN
F 1 "VCC" H 3900 3450 50  0000 C CNN
F 2 "" H 3900 3300 50  0000 C CNN
F 3 "" H 3900 3300 50  0000 C CNN
	1    3900 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3400 3900 3400
Wire Wire Line
	3900 3400 3900 3300
Text Label 3900 4100 2    60   ~ 0
nRESET
Text Label 3900 4000 2    60   ~ 0
SWO
Text Label 3900 3800 2    60   ~ 0
SWCLK
Text Label 3900 3700 2    60   ~ 0
SWDIO
NoConn ~ 4000 3500
NoConn ~ 4000 3600
NoConn ~ 4000 3900
NoConn ~ 4000 4200
NoConn ~ 4000 4300
NoConn ~ 4500 3400
Wire Wire Line
	3900 3700 4000 3700
Wire Wire Line
	4000 3800 3900 3800
Wire Wire Line
	3900 4000 4000 4000
Wire Wire Line
	4000 4100 3900 4100
$Comp
L VCC #PWR03
U 1 1 57AACD96
P 5750 3400
F 0 "#PWR03" H 5750 3250 50  0001 C CNN
F 1 "VCC" H 5750 3550 50  0000 C CNN
F 2 "" H 5750 3400 50  0000 C CNN
F 3 "" H 5750 3400 50  0000 C CNN
	1    5750 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3400 5750 3500
Wire Wire Line
	5750 3500 5850 3500
$Comp
L GND #PWR04
U 1 1 57AACDC7
P 5750 4100
F 0 "#PWR04" H 5750 3850 50  0001 C CNN
F 1 "GND" H 5750 3950 50  0000 C CNN
F 2 "" H 5750 4100 50  0000 C CNN
F 3 "" H 5750 4100 50  0000 C CNN
	1    5750 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4100 5750 3900
Wire Wire Line
	5750 3900 5850 3900
Wire Wire Line
	5850 3600 5650 3600
Wire Wire Line
	5850 3700 5650 3700
Wire Wire Line
	5850 3800 5650 3800
Wire Wire Line
	5850 4000 5650 4000
Text Label 5650 3600 2    60   ~ 0
SWDIO
Text Label 5650 3800 2    60   ~ 0
SWCLK
Text Label 5650 3700 2    60   ~ 0
nRESET
Text Label 5650 4000 2    60   ~ 0
SWO
$Comp
L RJ12 J1
U 1 1 57AB8F85
P 6300 3700
F 0 "J1" H 6500 4200 50  0000 C CNN
F 1 "RJ12" H 6150 4200 50  0000 C CNN
F 2 "RJ12_ISCP:RJ12_ISCP" H 6300 3700 50  0001 C CNN
F 3 "" H 6300 3700 50  0000 C CNN
	1    6300 3700
	0    1    1    0   
$EndComp
$Comp
L GND #PWR05
U 1 1 57AB8FC5
P 6500 4400
F 0 "#PWR05" H 6500 4150 50  0001 C CNN
F 1 "GND" H 6500 4250 50  0000 C CNN
F 2 "" H 6500 4400 50  0000 C CNN
F 3 "" H 6500 4400 50  0000 C CNN
	1    6500 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4400 6500 4300
Wire Wire Line
	6500 4300 6700 4300
Wire Wire Line
	6700 4300 6700 4200
Wire Wire Line
	6600 4200 6600 4300
Connection ~ 6600 4300
$EndSCHEMATC
